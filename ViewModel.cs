﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace JoesAutomotive
{
    class ViewModel : INotifyPropertyChanged
    {

        #region Initialization
        const decimal Oil_Change = 26.00m;
        const decimal lube_Job = 18.00m;
        const decimal radiator_Flush = 30.00m;
        const decimal transmission_Flush = 80.00m;
        const decimal car_inspection = 15.00m;
        const decimal muffler_Replacement = 100.00m;
        const decimal tire_Rotation = 20.00m;
        const decimal Hourly_Rate = 20.00m;
        const decimal Tax_Rate = 0.06m;

        private decimal labour;
        private decimal parts;
        private decimal total;
        private decimal tax;

        private bool oilChange;
        private bool lubeJob;
        private bool radiatorFlush;
        private bool transmissionFlush;
        private bool inspection;
        private bool mufflerReplacement;
        private bool tireRotation;

        #endregion

        #region Properties
        public bool OilChange
        {
            get
            {
                return oilChange;
            }
            set
            {
                oilChange = value;
                propertyChanged();
            }
        }

        public bool LubeJob
        {
            get
            {
                return lubeJob;
            }
            set
            {
                lubeJob = value;
                propertyChanged();
            }
        }

        public bool RadiatorFlush
        {
            get
            {
                return radiatorFlush;
            }
            set
            {
                radiatorFlush = value;
                propertyChanged();
            }
        }

        public bool TransmissionFlush
        {
            get
            {
                return transmissionFlush;
            }
            set
            {
                transmissionFlush = value;
                propertyChanged();
            }
        }

        public bool Inspection
        {
            get
            {
                return inspection;
            }
            set
            {
                inspection = value;
                propertyChanged();
            }
        }

        public bool MufflerReplacement
        {
            get
            {
                return mufflerReplacement;
            }
            set
            {
                mufflerReplacement = value;
                propertyChanged();
            }
        }

        public bool TireRotation
        {
            get
            {
                return tireRotation;
            }
            set
            {
                tireRotation = value;
                propertyChanged();
            }
        }

        public decimal Labour
        {
            get
            {
                return labour;
            }
            set
            {
                labour = value;
                propertyChanged();
            }
        }

        public decimal Parts
        {
            get
            {
                return parts;
            }
            set
            {
                parts = value;
                propertyChanged();
            }
        }

        public decimal Taxs
        {
            get
            {
                return tax;
            }
            set
            {
                tax = value;
                propertyChanged();
            }
        }

        public decimal Total
        {
            get
            {
                return total;
            }
            set
            {
                total = value;
                propertyChanged();
            }
        }

        #endregion

        private decimal OilLubeCharges()
        {
            decimal Price1 = 0;
            decimal Price2 = 0;

            if (oilChange) Price1 = Oil_Change;

            if (lubeJob) Price2 = lube_Job;

            return Price1 + Price2;
        }

        private decimal FlushCharges()
        {
            decimal Price1 = 0;
            decimal Price2 = 0;

            if (radiatorFlush) Price1 = radiator_Flush;

            if (transmissionFlush) Price2 = transmission_Flush;

            return Price1 + Price2;
        }

        private decimal MiscCharges()
        {
            decimal Price1 = 0;
            decimal Price2 = 0;
            decimal Price3 = 0;

            if (inspection) Price1 = car_inspection;

            if (mufflerReplacement) Price2 = muffler_Replacement;

            if (tireRotation) Price3 = tire_Rotation;

            return Price1 + Price2 + Price3;
        }

        private decimal OtherCharges()
        {
            if (parts < 0) parts = 0;

            if (labour < 0) labour = 0;

            return parts + (labour * Hourly_Rate);
        }

        private decimal TaxCharges()
        {
            if (parts < 0) parts = 0;
            tax = parts * Tax_Rate;
            return tax;

        }

        private decimal TotalCharges()
        {
            return OilLubeCharges() + FlushCharges() + MiscCharges() + OtherCharges() + TaxCharges();
        }

        public void ClearOilLube()
        {
            OilChange = false;
            LubeJob = false;
        }

        public void ClearFlushes()
        {
            RadiatorFlush = false;
            TransmissionFlush = false;
        }

        public void ClearMisc()
        {
            Inspection = false;
            MufflerReplacement = false;
            TireRotation = false;
        }

        public void ClearOther()
        {
            Parts = 0;
            Labour = 0;
        }

        public void ClearTaxandTotal()
        {
            Taxs = 0;
            Total = 0;
        }

        public void TotalAmount()
        {
            total = TotalCharges();

            propertyChanged("Total");
            propertyChanged("Taxs");
        }

        public void Clear()
        {
            ClearOilLube();
            ClearFlushes();
            ClearMisc();
            ClearOther();
            ClearTaxandTotal();

        }

        #region Property Changed
        public event PropertyChangedEventHandler PropertyChanged;

        private void propertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}